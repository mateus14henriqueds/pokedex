//
//  pokedex.swift
//  teste
//
//  Created by COTEMIG on 02/06/22.
//

import UIKit

class Pokedex: UIViewController, UITableViewDataSource {
    
    struct Pokemom {
        var Nome : String
        var tipo1 : String
        var tipo2 : String
        var id : String
        var img_pokemon : String
    }
    
    private var listaDePokemom = [Pokemom]()
    @IBOutlet weak var tableview: UITableView!
    
        override func viewDidLoad() {
        super.viewDidLoad()
            tableview.dataSource = self
            self.IniciarLista()
            
        }
    
    func IniciarLista() {
        self.listaDePokemom = [
            Pokemom(Nome: "Bulbassauro", tipo1: "Grama", tipo2: "Veneno", id: "#001", img_pokemon: "1"),
            Pokemom(Nome: "Ivyssauro", tipo1: "Grama", tipo2: "Veneno", id: "#002", img_pokemon: "2"),
            Pokemom(Nome: "Venussauro", tipo1: "Grama", tipo2: "Veneno", id: "#003", img_pokemon: "3"),
            Pokemom(Nome: "Charmander", tipo1: "Fogo", tipo2: "1", id: "#004", img_pokemon: "4"),
            Pokemom(Nome: "Charmeleon", tipo1: "Fogo", tipo2: "1", id: "#005", img_pokemon: "5"),
            Pokemom(Nome: "Charizard", tipo1: "Fogo", tipo2: "Voador", id: "#006", img_pokemon: "6"),
            Pokemom(Nome: "Squirtle", tipo1: "Água", tipo2: "1", id: "#007", img_pokemon: "7"),
            Pokemom(Nome: "Wartortle", tipo1: "Água", tipo2: "1", id: "#008", img_pokemon: "8"),
            Pokemom(Nome: "Blastoise", tipo1: "Água", tipo2: "1", id: "#009", img_pokemon: "9"),
            Pokemom(Nome: "Pikachu", tipo1: "Elétrico", tipo2: "1", id: "#010", img_pokemon: "10")
        
        ]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return listaDePokemom.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "celula_pokemon", for: indexPath) as? TableViewCell_pokemons_ {
            cell.Nome.text = self.listaDePokemom[indexPath.row].Nome
            cell.tipo1.text = self.listaDePokemom[indexPath.row].tipo1
            cell.tipo2.text = self.listaDePokemom[indexPath.row].tipo2
            cell.img_pokemon.image = UIImage(named:self.listaDePokemom[indexPath.row].img_pokemon)
            cell.id.text = self.listaDePokemom[indexPath.row].id
            
            return cell

    }

    
        func SAIR(_ sender: Any) {
        self.dismiss(animated: true)
        }
  return UITableViewCell()
}
}
